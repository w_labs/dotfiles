return {

	-- Mason LSP

	{
		"williamboman/mason-lspconfig.nvim",
		opts = {
			auto_install = true,
		},
		config = function()
			require("mason-lspconfig").setup({
				ensure_installed = {
					"bashls",
					"dockerls",
					"gopls",
					"lua_ls",
					"svelte",
					"tailwindcss",
					"terraformls",
					"vtsls",
				},
			})
		end,
	},

	-- Neovim LSP
	{
		"neovim/nvim-lspconfig",
		event = { "BufReadPost", "BufWritePost", "BufNewFile" },
		config = function()
			local caps = require("cmp_nvim_lsp").default_capabilities()

			local lsp = require("lspconfig")
			lsp.bashls.setup({
				capabilities = caps,
			})
			lsp.dockerls.setup({
				capabilities = caps,
			})
			lsp.gopls.setup({
				capabilities = caps,
			})
			lsp.lua_ls.setup({
				capabilities = caps,
			})
			lsp.tailwindcss.setup({
				capabilities = caps,
			})
			lsp.terraformls.setup({
				capabilities = caps,
			})
			lsp.svelte.setup({
				capabilities = caps,
			})
			lsp.vtsls.setup({
				capabilities = caps,
			})
		end,
	},
	-- Tools
	{
		"williamboman/mason.nvim",
		lazy = false,
		config = function()
			require("mason").setup()
		end,
	},
}
