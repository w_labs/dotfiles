set -g fish_greeting

if status is-interactive
end

# Alias
alias cat="bat"
alias ls="exa"
alias zed="zeditor"

# cd shortcut
abbr .. 'cd ..'
abbr ... 'cd ../..'
abbr .3 'cd ../../..'
abbr .4 'cd ../../../..'
abbr .5 'cd ../../../../..'

# Always mkdir
abbr mkdir 'mkdir -p'
