# wlabs - dotfiles


## Fix application menu

- Copy `config/menus` to `~/.config`, then run:

```shell
kbuildsycoca6
```

## Fix no file chooser

- Since `xdg-desktop-portal-hyprland` does not come with file chooser so you will need a fallback XDG Desktop Portal. Here we will use `xdg-desktop-portal-gtk`.

```shell
sudo pacman -S xdg-desktop-portal-gtk
```
