# dotfiles - Changelog

## 18-07-24

- Add `fish` config
- Add `Dolphin` config
- Add `Zed` config
- Fix `Open Terminal Here` in Dolphin
- Fix `Dolpin` Open with/application menu

## 06-05-2024

- Init
